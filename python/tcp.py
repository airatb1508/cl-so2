import io
import time
import pickle
import websocket
import uuid
from mitmproxy.utils import strutils
from mitmproxy import ctx
from mitmproxy import tcp
from websocket import create_connection
from protobuf_decoder import Parser
from protobuf_inspector.types import StandardParser
from datetime import datetime



# is_one = False
# log = create_connection("ws://localhost:7777/")
# handler = create_connection("ws://localhost:8765/")


print("script started")

parser = StandardParser() #1
last_still_here = datetime.now()

start_time = datetime.now()
sent = False


def tcp_message(flow: tcp.TCPFlow):
    global last_still_here
    global start_time
    global sent

    msg = flow.messages[-1]

    i_am_still_here = b'\x00\x00\x00\x01\x01'

    # if (msg.from_client):
    #     msg.content = i_am_still_here
    #     return


    # if (datetime.now() - last_still_here).total_seconds() == 25:
    #     last_still_here = datetime.now()
    #     flow.messages.append(TCPMessage(True, i_am_still_here))



    # check to sent previous (random) message descriptor
    # check package insertion
    # if msg.from_client and  sent == False and (datetime.now() - start_time).total_seconds() > 5:
    #     sent = True
    #     insert_message = b'\x00\x00\x00f\n$' + str(uuid.uuid4()).encode() + b'\x12\x18MarketplaceRemoteService\x1a\x15createPurchaseRequest"\r\x1a\x0b\x08\x94\xb0\n\x15\n\xd7\xa3=\x18\x01'
    #     msg.content = insert_message
    #     print("its alive!  " * 180)


    mod = msg.content.split(b"\n", 1)
    if len(mod) == 2:
        mod = b"\n" + mod[1]
    else:
        mod = mod[0]

    meta = ("client:" + str(flow.client_conn.address[0]) +
            " server:" + str(flow.server_conn.address[0]) +
            " from-client:" + ("t" if msg.from_client else "nil") +
            " msg:").encode()

    # handler.send(meta + mod, websocket.ABNF.OPCODE_BINARY) #websocket.ABNF.OPCODE_BINARY as second arg
    # res = handler.recv()


    if (msg.from_client):
        print("\n\n\n\n\n>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> from user")
    else:
        print("\n\n\n\n\n<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<   to user")
    print(msg.content)
    print("==================================================")
    try:
        output = Parser().parse(mod.hex())
        print(output)
    except:
        pass

    print("==================================================")
    try:
        output = parser.parse_message(io.BytesIO(mod), "message")
        print(output)
    except:
        pass

    print((datetime.now() - start_time).total_seconds())
