(ql:quickload :cl-ppcre)
(load "utils/string.lisp")
(load "proto/protobuf.lisp")
(load "proto/protoparser.lisp")


(defparameter *server-ips* '("18.153.0.126"  "3.71.250.82"    "3.68.243.227"  "3.71.161.250"
                             "18.158.140.46" "18.157.144.214" "3.125.105.137" "3.120.94.151"
                             "18.197.6.106"))


(defun msg-meta-length (msg)
  (+ 4 (search (string->bytelist "msg:") msg)))


(defun msg-meta-str (msg)
  (bytelist->string (subseq msg 0 (msg-meta-length msg))))


(defun parse-msg (msg)
  (let ((meta (cl-ppcre:split #\Space (msg-meta-str msg))))
    (list
     :client (cadr (cl-ppcre:split #\: (car meta)))
     :server (cadr (cl-ppcre:split #\: (cadr meta)))
     :from-clientp (string= "t" (cadr (cl-ppcre:split #\: (caddr meta))))
     :msg (msg-without-meta msg))))


(defun msg-without-meta (msg)
  (subseq msg (msg-meta-length msg)))


(defun message-handler (msg)
  (let ((parsed (parse-msg msg)))
    (format t "~A~%" (cadr (member :server parsed)))
    ;; (if (and (member (cadr (member :server parsed)) *server-ips* :test #'string=)
    ;;          (not (cadr (member :from-clientp parsed))))
    (proto-parser (make-protobuf (cadr (member :msg parsed))))))
    ;; )

(defparameter msg '(ON-TRADE-UPDATE
                    ((BODY
                      (UPDATE
                       (VALUES (ITEM-ID . 133501) (SELL-COUNT . 2085) (BUY-COUNT . 52083)
                        (BUY-PRICE . 11.0) (SELL-PRICE . 9.52)))))))
