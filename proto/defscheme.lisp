(load "proto/protobuf")
(load "proto/proto-types")


(defun expand-scheme (body)
  (if (eq 'cons (type-of (car body)))
      `(nconc ,@(mapcar #'(lambda (item) (expand-scheme item)) body))
      `(list ,(car body)
             (ignore-errors
               (parse-wire-type pb)
               ,(case (cadr body)
                  (varint '(parse-varint  pb))
                  (string '(parse-string  pb))
                  (bit32  '(parse-float32 pb))
                  (bit64  '(parse-float64 pb))
                  (chunk  `(let ((pb (parse-chunk pb)))
                             (when (> (len pb) 0)
                               ,(expand-scheme (cddr body))))))))))


(defmacro defscheme (name params &rest body)
  `(defun ,name (pb)
     (list
      ,(intern (string name) :keyword)
      ,(expand-scheme body))))
