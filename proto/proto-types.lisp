(ql:quickload :ieee-floats)
(load "proto/protobuf.lisp")
(load "utils/string.lisp")


(defun has-nextp (byte)
  (/= 0 (logand byte #x80)))


(defun varint-value (bytes)
  (let ((value 0))
    (loop for i from 0
          and byte in bytes
          do (setf value (+ value (ash (logand #x7F byte) (* 7 i)))))
    value))


(defun parse-varint (pb)
  (let ((byte)
        (bytes))
    (setf bytes (loop do (setf byte (read-one pb))
                      collect byte
                      while (has-nextp byte)))
    (varint-value bytes)))


(defun float-value (bytes)
  (let ((value 0))
    (loop for i from 0
          and byte in bytes
          do (setf value (+ value (ash byte (* 8 i)))))
    value))


(defun parse-float32 (pb)
  (ieee-floats:decode-float32 (float-value (read-n pb 4))))


(defun parse-float64 (pb)
  (ieee-floats:decode-float64 (float-value (read-n pb 8))))


(defun parse-delimited (pb)
  (let ((n (parse-varint pb)))
    (read-n pb n)))


(defun parse-chunk (pb)
  (make-instance 'protobuf :buffer (parse-delimited pb)))


(defun parse-string (pb)
  (bytelist->string (parse-delimited pb)))


(defun parse-wire-type (pb)
  (let ((byte (read-one pb)))
    (values (logand byte #x7) (ash byte -3))))


(defun int->wire-type (byte)
  (values (logand byte #x7) (ash byte -3)))
