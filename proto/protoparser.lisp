(ql:quickload '(alexandria))
(load "proto/protobuf")
(load "proto/proto-types")
(load "proto/schemes/marketplace")


(defun mrel (pb)
  (when (= 2 (parse-wire-type pb))
    (alexandria:switch ((parse-string pb) :test #'equal)
      ("onTradeUpdated" (on-trade-update pb)))))


(defun parse (pb)
  (when (= 2 (parse-wire-type pb))
    (alexandria:switch ((parse-string pb) :test #'equal)
      ("MarketplaceRemoteEventListener" (mrel pb)))))


(defun proto-parser (pb)
  (format t ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>~%")
  ;; (with-slots (buffer) pb
  ;;   (format t "~a~%" (bytelist->string buffer))
  ;;   (format t "~a~%" buffer))
  (format t "~a~%" (parse pb)))
