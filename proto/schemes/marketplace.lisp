(load "proto/defscheme")


(defscheme on-trade-update ()
  (:body chunk
        (:update chunk
                (:values chunk
                        (:item-id    varint)
                        (:sell-count varint)
                        (:buy-count  varint)
                        (:buy-price  bit32)
                        (:sell-price bit32)))))


(defscheme on-trade-request-closed ()
  (:body chunk
        (:3-chunk chunk
                 (:1-chunk chunk
                          (:id string)
                          (:originId string)
                          (:player  chunk
                                  (:id string)
                                  (:uid string)
                                  (:name string)
                                  (:avatar-id varint)
                                  (:client chunk
                                          (:info chunk
                                                (:some-id string)
                                                (:version string))
                                          (:server varint))
                                   (:registration-date? varint))
                          (:item-id varint)
                          (:price bit32)
                          (:create-date varint)
                          (:close-date varint)
                          (:8-var varint)
                          (:11-var varint)))))

;; // Fields
;; private static readonly MessageParser<ClosedRequest> _parser; // 0x0
;; private string id_; // 0x8
;; private string originId_; // 0xC
;; private Player creator_; // 0x10
    ;; private static readonly MessageParser<Player> _parser; // 0x0
    ;; private string id_; // 0x8
    ;; private string uid_; // 0xC
    ;; private string name_; // 0x10
    ;; private string avatarId_; // 0x14
    ;; private int timeInGame_; // 0x18
    ;; private PlayerStatus playerStatus_; // 0x1C
    ;; private long logoutDate_; // 0x20
    ;; private long registrationDate_; // 0x28
    ;; private Attributes attributes_; // 0x30
    ;; private string testerRole_; // 0x34
    ;; private static readonly FieldCodec<PlayerBan> _repeated_bans_codec; // 0x4
    ;; private readonly RepeatedField<PlayerBan> bans_; // 0x38
;; private int itemDefinitionId_; // 0x14
;; private float price_; // 0x18
;; private long createDate_; // 0x20
;; private long closeDate_; // 0x28
;; private MarketRequestType type_; // 0x30
;; private Player partner_; // 0x34
;; private string partnerRequestId_; // 0x38
;; private ClosingReason reason_; // 0x3C
;; private int quantity_; // 0x40
;; private static readonly MapField.Codec<string, InventoryItemProperty> _map_properties_codec; // 0x4
;; private readonly MapField<string, InventoryItemProperty> properties_; // 0x44



(defscheme on-trade-request-opened ()
  (:3-chunk chunk
           (:3-chunk chunk
                    (:1-chunk chunk
                             (:hash string)
                             (:2-chunk chunk
                                      (:hash string)
                                      (:uid string)
                                      (:uname string)
                                      (:aaaa varint)
                                      (:client-info chunk
                                               (:2-chunk chunk
                                                        (:1-str string)
                                                        (:version string))
                                               (:3-var varint))
                                      (:7-var varint)
                                      (:8-var varint))
                             (:3-var varint)
                             (:4-32  bit32)
                             (:5-var varint)
                             (:6-smt varint)))))

;; // Fields
;; private static readonly MessageParser<OpenRequest> _parser; // 0x0
;; private string id_; // 0x8
;; private Player creator_; // 0xC
;;     private static readonly MessageParser<Player> _parser; // 0x0
;;     private string id_; // 0x8
;;     private string uid_; // 0xC
;;     private string name_; // 0x10
;;     private string avatarId_; // 0x14
;;     private int timeInGame_; // 0x18
;;     private PlayerStatus playerStatus_; // 0x1C
;;     private long logoutDate_; // 0x20
;;     private long registrationDate_; // 0x28
;;     private Attributes attributes_; // 0x30
;;     private string testerRole_; // 0x34
;;     private static readonly FieldCodec<PlayerBan> _repeated_bans_codec; // 0x4
;;     private readonly RepeatedField<PlayerBan> bans_; // 0x38
;; private int itemDefinitionId_; // 0x10
;; private float price_; // 0x14
;; private long createDate_; // 0x18
;; private MarketRequestType type_; // 0x20
;; private int quantity_; // 0x24
;; private static readonly MapField.Codec<string, InventoryItemProperty> _map_properties_codec; // 0x4
;; private readonly MapField<string, InventoryItemProperty> properties_; // 0x28

;; message:
;;     1 <chunk> = "MarketplaceRemoteEventListener"
;;     2 <chunk> = "onTradeRequestOpened"
;;     3 <chunk> = message:
;;         3 <chunk> = message:
;;             1 <chunk> = message:
;;                 1 <chunk> = "626440419baa7777953c8c0e"
;;                 2 <chunk> = message:
;;                     1 <chunk> = "6255ce40f4b0b9715eaddd4f"
;;                     2 <chunk> = message:
;;                         6 <64bit> = 0x3431363830333833 / 3760846778785806387 / 2.7419971e-57
;;                     3 <chunk> = "dober man"
;;                     5 <varint> = 36349
;;                     6 <chunk> = message:
;;                         2 <chunk> = message:
;;                             1 <chunk> = "1"
;;                             2 <chunk> = "0.19.0"
;;                         3 <varint> = 1
;;                     7 <varint> = 1650724358097
;;                     8 <varint> = 1649790528013
;;                     9 <chunk> = empty chunk
;;                 3 <varint> = 11006
;;                 4 <32bit> = 0x3EDC28F6 / 1054615798 / 0.430000
;;                 5 <varint> = 1650737217126
;;                 6 <varint> = 1







;; createPurchaseRequest создание запроса
;; b'\x00\x00\x00f\n$2dca89c6-b63f-4e87-a231-53c92391c4e4\x12\x18MarketplaceRemoteService\x1a\x15createPurchaseRequest"\r\x1a\x0b\x08\x94\xb0\n\x15\n\xd7\xa3=\x18\x01'
;; 1 <string>: 2dca89c6-b63f-4e87-a231-53c92391c4e4
;; 2 <string>: MarketplaceRemoteService
;; 3 <string>: createPurchaseRequest
;; 4 <length_delimited>:
;;     3 <length_delimited>:
;;         1 <varint>: 170004 // item-id
;;         2 <32>: 0.08       // price
;;         3 <varint>: 1      // quantity

;; вот такой ответ что создалось
;; b'\x00\x00\x00F\nD\n$2dca89c6-b63f-4e87-a231-53c92391c4e4\x1a\x1c\x1a\x1a\n\x18626ee5034d7d5858b15ad3c4'
;; ==================================================
;;     1 <length_delimited>:
;;         1 <string>: 2dca89c6-b63f-4e87-a231-53c92391c4e4
;;         3 <length_delimited>:
;;             3 <length_delimited>:
;;                 1 <string>: 626ee5034d7d5858b15ad3c4

;; вот такая тема что удалось купить
;; '\x00\x00\x01\xd2\x12\xcf\x03\n\x1eMarketplaceRemoteEventListener\x12\x15onPlayerRequestClosed\x1a\x95\x03\x1a\x92\x03\n\xfe\x02\n$4351dbf5-559c-412a-9d0c-3dad94f7c390\x12\x18626ee5034d7d5858b15ad3c4\x1a\x95\x01\n\x185d792d3e735dcb5a847d1618\x12\x0854029259\x1aQ\xe1\x80\xaa\xe1\x80\xaa\xe1\x80\xaa\xe1\x80\xaa\xe1\x80\xaa\xe1\x80\xaa\xe1\x80\xaa\xe1\x80\xaa\xe1\x80\xaa\xe1\x80\xaa\xe1\x80\xaa\xe1\x80\xaa\xe1\x80\xaa\xe1\x80\xaa\xe1\x80\xaa\xe1\x80\xaa\xe1\x80\xaa\xe1\x80\xaa\xe1\x80\xaa\xe1\x80\xaa\xe1\x80\xaa\xe1\x80\xaa\xe1\x80\xaa\xe1\x80\xaa\xe1\x80\xaa\xe1\x80\xaa\xe1\x80\xaa(\xe5\xdf\x112\x0f\x12\x0b\n\x011\x12\x060.19.0\x18\x018\x89\xfc\xb3\x89\x880J\x00 \x94\xb0\n-\n\xd7\xa3=0\xbc\xac\xba\x89\x8808\xe3\xac\xba\x89\x880@\x02Ja\n\x1860a68e5e315c4944e9f38974\x12\t104889343\x1a\x15\xd0\x9a\xd0\xbe\xd0\xbc\xd0\xb0\xd0\xbd\xd0\xb4\xd0\xb0 \xd0\xa2\xd1\x8f\xd0\xbd(\x9c\xf7\x1e2\x0f\x12\x0b\n\x011\x12\x060.19.0\x18\x018\xea\xd9\xb0\x88\x880@\xca\xc3\xf0\xd4\x98/J\x00R$6e9ccc92-1079-4dd7-9f15-a58996cd957aX\x01`\x01\x12\x0f\x08\x03\x10\x94\xb0\n\x18\x01(\xe6\xac\xba\x89\x880\x00\x00\x01\x8e\x12\x8b\x03\n\x1eMarketplaceRemoteEventListener\x12\x14onTradeRequestClosed\x1a\xd2\x02\x1a\xcf\x02\n\xcc\x02\n$42775eae-e762-4f8f-92e0-7dd4b99c82f3\x12\x18626ee4ead3a59c0d0b0d4364\x1an\n\x18596a8e41cd10e8098d753178\x12\x0810929098\x1a\x10Miyamoto Musashi(\xca\xad72)\x12%\n\x011\x12\x060.19.0\x1a\x18626ee379ab291703ce7bcdf9\x18\x018\x90\xa6\x9e\x89\x880J\x00 \x94\xb0\n-\x8f\xc2\xf5<0\x94\xe6\xb8\x89\x8808\xc6\xac\xba\x89\x880@\x02JW\n\x185c9cc116dc8c97536c5f363e\x12\x0842094417\x1a\x12\xd0\xa4\xd0\xb5\xd0\xb54ka \xd0\xb1\xd0\x9b\xd1\x83\xd0\x9c(\x9a\xc1\xe5\x012\x0f\x12\x0b\n\x011\x12\x060.19.0\x18\x018\x91\xc2\xd1\x88\x880J\x00R$334ac050-63e6-4ea7-9d63-9a3cd05f6b6dX\x01`\x01'
;; private ClosedRequest request_; // 0x8
;; private PlayerInventoryItem item_; // 0xC
;;     private int id_; // 0x8
;;     private int itemDefinitionId_; // 0xC
;;     private int quantity_; // 0x10
;;     private int flags_; // 0x14
;;     private long date_; // 0x18
;;     private static readonly MapField.Codec<string, InventoryItemProperty> _map_properties_codec; // 0x4
;;     private readonly MapField<string, InventoryItemProperty> properties_; // 0x20
;;         private PropertyType type_; // 0x8
;;         private int intValue_; // 0xC
;;         private float floatValue_; // 0x10
;;         private string stringValue_; // 0x14
;;         private bool booleanValue_; // 0x18




;; и обычная тема что прошел трейд
;; b'\x00\x00\x01\xc0\x12\xbd\x03\n\x1eMarketplaceRemoteEventListener\x12\x14onTradeRequestClosed\x1a\x84\x03\x1a\x81\x03\n\xfe\x02\n$6e9ccc92-1079-4dd7-9f15-a58996cd957a\x12\x18626ee44c60428b623de619dd\x1aa\n\x1860a68e5e315c4944e9f38974\x12\t104889343\x1a\x15\xd0\x9a\xd0\xbe\xd0\xbc\xd0\xb0\xd0\xbd\xd0\xb4\xd0\xb0 \xd0\xa2\xd1\x8f\xd0\xbd(\x9c\xf7\x1e2\x0f\x12\x0b\n\x011\x12\x060.19.0\x18\x018\xea\xd9\xb0\x88\x880@\xca\xc3\xf0\xd4\x98/J\x00 \x94\xb0\n-\x8f\xc2u=0\x98\x99\xaf\x89\x8808\xe3\xac\xba\x89\x880@\x01J\x95\x01\n\x185d792d3e735dcb5a847d1618\x12\x0854029259\x1aQ\xe1\x80\xaa\xe1\x80\xaa\xe1\x80\xaa\xe1\x80\xaa\xe1\x80\xaa\xe1\x80\xaa\xe1\x80\xaa\xe1\x80\xaa\xe1\x80\xaa\xe1\x80\xaa\xe1\x80\xaa\xe1\x80\xaa\xe1\x80\xaa\xe1\x80\xaa\xe1\x80\xaa\xe1\x80\xaa\xe1\x80\xaa\xe1\x80\xaa\xe1\x80\xaa\xe1\x80\xaa\xe1\x80\xaa\xe1\x80\xaa\xe1\x80\xaa\xe1\x80\xaa\xe1\x80\xaa\xe1\x80\xaa\xe1\x80\xaa(\xe5\xdf\x112\x0f\x12\x0b\n\x011\x12\x060.19.0\x18\x018\x89\xfc\xb3\x89\x880J\x00R$4351dbf5-559c-412a-9d0c-3dad94f7c390X\x01`\x01'
;; ==================================================
;;     1 <string>: MarketplaceRemoteEventListener
;;     2 <string>: onTradeRequestClosed
;;     3 <length_delimited>:
;;         3 <length_delimited>:
;;             1 <length_delimited>:
;;                 1 <string>: 6e9ccc92-1079-4dd7-9f15-a58996cd957a
;;                 2 <string>: 626ee44c60428b623de619dd
;;                 3 <length_delimited>:
;;                     1 <string>: 60a68e5e315c4944e9f38974
;;                     2 <length_delimited>:
;;                         6 <64>: 0.0
;;                     3 <string>: Команда Тян
;;                     5 <varint>: 506780
;;                     6 <length_delimited>:
;;                         2 <length_delimited>:
;;                             1 <string>: 1
;;                             2 <length_delimited>:
;;                                 6 <varint>: 46
;;                         3 <varint>: 1
;;                     7 <varint>: 1651432500458
;;                     8 <varint>: 1621528158666
;;                 4 <varint>: 170004
;;                 5 <32>: 0.06
;;                 6 <varint>: 1651434572952
;;                 7 <varint>: 1651434755683
;;                 8 <varint>: 1
;;                 9 <length_delimited>:
;;                     1 <string>: 5d792d3e735dcb5a847d1618
;;                     2 <length_delimited>:
;;                         6 <32>: 0.0
;;                     3 <string>: my name here
;;                     5 <varint>: 290789
;;                     6 <length_delimited>:
;;                         2 <length_delimited>:
;;                             1 <string>: 1
;;                             2 <length_delimited>:
;;                                 6 <varint>: 46
;;                         3 <varint>: 1
;;                     7 <varint>: 1651434651145
;;                 10 <string>: 4351dbf5-559c-412a-9d0c-3dad94f7c390
;;                 11 <varint>: 1
;;                 12 <varint>: 1





;; login trough `GoogleAuthRemoteService`
;; public class GoogleAuthRemoteService : RpcService // TypeDefIndex: 6396
;; {
;;  // Methods

;;  // RVA: 0xDBC52C Offset: 0xDBC52C VA: 0xDBC52C
;;  public void .ctor(ClientService client) { }

;;  [RpcAttribute] // RVA: 0x5A3D68 Offset: 0x5A3D68 VA: 0x5A3D68
;;  // RVA: 0xDBC534 Offset: 0xDBC534 VA: 0xDBC534
;;  public GoogleLinkAuthResponse LinkAuth(GoogleLinkAuthRequest request, CancellationToken ct) { }

;;  [RpcAttribute] // RVA: 0x5A3DA0 Offset: 0x5A3DA0 VA: 0x5A3DA0
;;  // RVA: 0xDBC680 Offset: 0xDBC680 VA: 0xDBC680
;;  public GoogleAuthResponse EncryptedAuth(GoogleAuthRequest request, CancellationToken ct) { }

;;  [RpcAttribute] // RVA: 0x5A3DD8 Offset: 0x5A3DD8 VA: 0x5A3DD8
;;  // RVA: 0xDBC7CC Offset: 0xDBC7CC VA: 0xDBC7CC
;;  public GoogleUnLinkAuthResponse UnLinkAuth(GoogleUnLinkAuthRequest request, CancellationToken ct) { }
;; }
