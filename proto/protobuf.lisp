(defclass protobuf ()
  ((buffer
    :initarg :buffer
    :initform nil
    :accessor buffer)

   (pos
    :initarg :pos
    :initform 0
    :accessor pos)))


(defun make-protobuf (buffer)
  (make-instance
   'protobuf
   :buffer (let ((buf (if (eq (type-of buffer) 'list)
                          buffer
                       (coerce buffer 'list))))
             (if (< (or (position 10 buf) 100) 16)
                 (subseq buf (position 10 buf))
                 buf))))


(defmethod read-n ((obj protobuf) n)
  (with-slots (buffer pos) obj
    (setf pos (+ pos n))
    (subseq buffer (- pos n) pos)))


(defmethod read-one ((obj protobuf))
  (car (read-n obj 1)))


(defmethod len ((obj protobuf))
  (with-slots (buffer pos) obj
    (length buffer)))
