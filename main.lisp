(ql:quickload '(clack websocket-driver alexandria))
(load "./utils/string.lisp")
(load "./message-handler/main.lisp")


(defun handle-open (connection)
  (wsd:send connection "Hello boi")
  (format t "New connection!~%"))


(defun handle-msg (connection msg)
  (ignore-errors
   (message-handler (coerce msg 'list)))
  (wsd:send connection "pong"))


(defun chat-server (env)
  (let ((ws (wsd:make-server env)))
    (wsd:on :open ws
            (lambda () (handle-open ws)))

    (wsd:on :message ws
            (lambda (msg) (handle-msg ws msg)))

    (wsd:on :close ws
            (lambda (&key code reason)
              (declare (ignore code reason))
              (format t "connection closed~%")))

    (lambda (responder)
      (declare (ignore responder))
      (wsd:start-connection ws))))


(defvar *chat-handler* (clack:clackup #'chat-server :port 8765))
;; (clack:close *chat-handler*)
