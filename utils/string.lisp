(defun string->bytearray (chars)
  (let ((vec (make-array (length chars))))
    (loop for char across chars
       for i from 0
       do (setf (aref vec i) (char-code char)))
    vec))

(defun bytearray->string (bytes)
  (let ((str (make-string (length bytes))))
    (loop for byte across bytes
       for i from 0
       do (setf (aref str i) (code-char byte)))
    str))

(defun string->bytelist (chars)
  (let ((lst (make-array (length chars))))
    (loop for char across chars
          for i from 0
          do (setf (aref lst i) (char-code char)))
    ;; ???
    (coerce lst 'list)))


(defun bytelist->string (bytes)
  (let ((str (make-string (length bytes))))
    (loop for byte in bytes
          for i from 0
          do (setf (aref str i) (code-char byte)))
    str))
