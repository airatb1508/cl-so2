(ql:quickload '(clack websocket-driver alexandria))

(defun handle-open (connection)
  (wsd:send connection "Hello")
  (format t "New connection!~%"))

(defun handle-msg (connection msg)
  (wsd:send connection "Got it")
  (format t "Log: ~a~%" msg))

(defun log-server (env)
  (let ((ws (wsd:make-server env)))
    (wsd:on :open ws
            (lambda () (handle-open ws)))

    (wsd:on :message ws
            (lambda (msg) (handle-msg ws msg)))

    (wsd:on :close ws
            (lambda (&key code reason)
              (declare (ignore code reason))
              (format t "Closed~%")))

    (lambda (responder)
      (declare (ignore responder))
      (wsd:start-connection ws))))

(defvar *log-handler* (clack:clackup #'log-server :port 7777))
;; (clack:close *log-handler*)
